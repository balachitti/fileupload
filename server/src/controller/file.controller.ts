
import  {Request,Response,NextFunction} from 'express';
import path = require('path')
import fs = require('fs')
const dir = '/home/aspire1222/Desktop/Desktop/task/file/server/uploads'
 
const uploadFile = (req: Request,res: Response)=> {
    console.log('function')
    if (!req.file) {
        console.log("No file received");
        return res.send({
          success: false
        });
    }
    else {
          if(fs.existsSync(dir+ '/' + `${req.file.filename}` ))
          {
            res.status(500).json({error:"already exist"})
          }else{
        console.log('file received successfully');
        return res.json([{
          success: true,
          path: `http://localhost:5000/api/view/${req.file.filename}`,
          message: 'File uploaded successfully',
          filename: req.file.filename
        }])
      }
    }
}
const downloadFile = (req: Request,res: Response) => {
    var file = req.params.file;
    var fileLocation = path.join(dir,file);
    console.log(fileLocation);
    res.download(fileLocation, file); 
  };
const view = (req: Request,res: Response) => {
  
  var response: { "name": string; size: number; date: Date}[]=[];
  fs.readdir('uploads', (err, files) => {
    
    files.forEach(file => {
      //console.log(file);
      var size = fs.statSync(dir+'/'+file).size;
      var date = fs.statSync(dir + '/' + file).ctime
      response.push({"name":file,size,date});

        
       
    });
    // response.sort( (a,b)=> { 
    //   return (b.name.localeCompare(a.name));
    // });
    console.log(response)
   res.send(response)
  });
   
   
}

export {
    uploadFile,
    downloadFile,
    view
}

