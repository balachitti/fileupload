import  express  = require('express');
import {Request,Response,NextFunction} from 'express';
import cors from "cors";

import * as bodyParser from 'body-parser'
import  {fileRoutes} from './routes/routes'
class App{
   public app: express.Application;
   constructor(){
      this.app = express();
      this.config();
      this.allRoutes();
      
       }
   private config(): void{
      this.app.use(bodyParser.json());
      //this.app.use(bodyParser.urlencoded({ extended: true }));
      this.app.use(cors());
    }
  
   private allRoutes(): void{
       
      this.app.use("/api", fileRoutes);
    //  console.log('/api');
      this.app.get('/',(req:Request, res:Response) => {
         //console.log("entered")
         res.json(`backend is ready`)
       })
    }
}
export default new App().app;