

import express, {Request,Response,NextFunction} from 'express';
import multer from 'multer';
import * as up from '../controller/file.controller'

const path ='/home/aspire1222/Desktop/Desktop/task/file/server' 

const router = express();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path + '/uploads/')
    },
    
    filename: function (req: any, file: any, cb: any) {
        cb(null, file.originalname)
    }
    
});

const upload = multer({storage: storage });
//console.log(__dirname)

router.post('/upload',upload.single('file') ,up.uploadFile)
router.get('/download/:file(*)',up.downloadFile)
router.get('/view',up.view)
router.use('/view',express.static('uploads'))


export {router as fileRoutes}


