"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var fs = require("fs");
var dir = '/home/aspire1222/Desktop/Desktop/task/file/server/uploads';
var uploadFile = function (req, res) {
    console.log('function');
    if (!req.file) {
        console.log("No file received");
        return res.send({
            success: false
        });
    }
    else {
        if (fs.existsSync(dir + '/' + ("" + req.file.filename))) {
            res.status(500).json({ error: "already exist" });
        }
        else {
            console.log('file received successfully');
            return res.json([{
                    success: true,
                    path: "http://localhost:5000/api/view/" + req.file.filename,
                    message: 'File uploaded successfully',
                    filename: req.file.filename
                }]);
        }
    }
};
exports.uploadFile = uploadFile;
var downloadFile = function (req, res) {
    var file = req.params.file;
    var fileLocation = path.join(dir, file);
    console.log(fileLocation);
    res.download(fileLocation, file);
};
exports.downloadFile = downloadFile;
var view = function (req, res) {
    var response = [];
    fs.readdir('uploads', function (err, files) {
        files.forEach(function (file) {
            //console.log(file);
            var size = fs.statSync(dir + '/' + file).size;
            var date = fs.statSync(dir + '/' + file).ctime;
            response.push({ "name": file, size: size, date: date });
        });
        // response.sort( (a,b)=> { 
        //   return (b.name.localeCompare(a.name));
        // });
        console.log(response);
        res.send(response);
    });
};
exports.view = view;
