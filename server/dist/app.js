"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var cors_1 = __importDefault(require("cors"));
var bodyParser = __importStar(require("body-parser"));
var routes_1 = require("./routes/routes");
var App = /** @class */ (function () {
    function App() {
        this.app = express();
        this.config();
        this.allRoutes();
    }
    App.prototype.config = function () {
        this.app.use(bodyParser.json());
        //this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(cors_1.default());
    };
    App.prototype.allRoutes = function () {
        this.app.use("/api", routes_1.fileRoutes);
        //  console.log('/api');
        this.app.get('/', function (req, res) {
            //console.log("entered")
            res.json("backend is ready");
        });
    };
    return App;
}());
exports.default = new App().app;
