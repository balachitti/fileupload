"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var multer_1 = __importDefault(require("multer"));
var up = __importStar(require("../controller/file.controller"));
var path = '/home/aspire1222/Desktop/Desktop/task/file/server';
var router = express_1.default();
exports.fileRoutes = router;
var storage = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path + '/uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var upload = multer_1.default({ storage: storage });
//console.log(__dirname)
router.post('/upload', upload.single('file'), up.uploadFile);
router.get('/download/:file(*)', up.downloadFile);
router.get('/view', up.view);
router.use('/view', express_1.default.static('uploads'));
