import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient) {}

  upload(data) {
    return this.http.post(environment.apiBaseUrl + '/upload', data);
  }
  list() {
    return this.http.get(environment.apiBaseUrl + '/view');
  }
  download(file) {
    window.open(environment.apiBaseUrl + `/download/${file}`);
  }

}
