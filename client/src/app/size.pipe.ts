import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'size'
})
export class SizePipe implements PipeTransform {
  transform(size: number) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    // tslint:disable-next-line: triple-equals
    if (size == 0) { return 'n/a'; }
    // tslint:disable-next-line:radix
    let i: number;
    i =  Math.floor(Math.log(size) / Math.log(1024));
    if (i == 0) { return size + ' ' + sizes[i]; }
    return (size / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
  }
}
