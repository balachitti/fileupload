import { FileService } from './file.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SizePipe } from './size.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SizePipe

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [FileService, SizePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
