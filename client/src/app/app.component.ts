import { SizePipe } from './size.pipe';
import { environment } from './../environments/environment';
import { FileService } from './file.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  files: any;
 msg: any;
  desc = true;
  buffer: any;
  myForm = new FormGroup({
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required])
  });
  err: any;

  constructor(private fileservice: FileService, private sizepipe: SizePipe) {
    this.readfile();
  }
  ngOnInit() {}
  readfile() {
    this.fileservice.list().subscribe(data => {
      console.log(data);
      this.files = data;
      this.buffer = data;
      console.log(this.buffer);
    });
  }

  get f() {
    return this.myForm.controls;
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      // this.myForm.patchValue({
      //   fileSource: file
      // });
      this.myForm.get('fileSource').setValue(file);
    }
  }

  download(data) {
    this.fileservice.download(data);
  }
  view(data) {
    window.open(environment.apiBaseUrl + `/view/${data}`);
  }
  submit() {
    const formData = new FormData();
    formData.append('file', this.myForm.get('fileSource').value);


    this.fileservice.upload(formData).subscribe(
      res => {
        //console.log(res);
        this.msg = res;
        console.log(this.msg);
            },
      err => {
        console.log(err.error);
        this.err = err.error;
        }
    );
    //setTimeout(() => { window.location.reload(); }, 4000);
   }
   listSort() {
     this.files = this.buffer;
    //  if (this.desc === true) {
    //    this.desc = false;
    //  } else {
    //     this.desc = true;
    //  }
     this.files.sort(( a, b) => {
      if (this.desc === true) {

        console.log(this.desc + 'up');
        return (b.name.localeCompare(a.name));
      } else {

        console.log(this.desc + 'down');
        return (a.name.localeCompare(b.name));
      }
    });
     if (this.desc === true) {
       this.desc = false;
     } else {
        this.desc = true;
     }
     console.log(this.files);
   }

   sizeSort() {
    this.files.sort(( a, b) => {
      if (this.desc === true) {

        console.log(this.desc + 'up');
        return (b.size - a.size);
      } else {

        console.log(this.desc + 'down');
        return (a.size - b.size);
      }
    });
    if (this.desc === true) {
       this.desc = false;
     } else {
        this.desc = true;
     }

   }
   // return Math.abs(new Date(a.date) - new Date(b.date))
   dateSort() {
    this.files.sort(( a, b) => {
      if (this.desc === true) {

        console.log(this.desc + 'up');
        return (+new Date(a.date) - +new Date(b.date));
      }
        else {
        console.log(this.desc + 'down');
        return (+new Date(b.date) - +new Date(a.date));
      }
    });
    if (this.desc === true) {
       this.desc = false;
     } else {
        this.desc = true;
     }

   }


}
